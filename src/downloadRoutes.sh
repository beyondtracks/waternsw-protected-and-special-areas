#!/bin/sh

input=$1
output=$2
mkdir -p $2

cat $1 | \
    grep -v '^#' | \
    grep -v '^$' | \
    sed 's/^/https:\/\/routing.openstreetmap.de\/routed-foot\/route\/v1\/driving\//' | \
    sed 's/$/?overview=full\&geometries=geojson\&alternatives=false\&steps=false/' | \
    wget -i - --directory-prefix $2

