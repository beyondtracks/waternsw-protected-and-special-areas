#!/usr/bin/env node

const fs = require('fs')
const combine = require('@turf/combine').default
const difference = require('@turf/difference')
const featureCollection = require('@turf/helpers').featureCollection

const placesYouCanGo = JSON.parse(fs.readFileSync('build/places-you-can-go.geojson'))
const specialAndControlledAreas = JSON.parse(fs.readFileSync('build/Special_and_Controlled_Areas.geojson'))

const negativeFeature = combine(placesYouCanGo).features[0]

const outputFeatures = specialAndControlledAreas.features.map((feature) => {
    const result = difference(feature, negativeFeature)
    return result
})

fs.writeFileSync('dist/Special_and_Controlled_Areas.geojson', JSON.stringify(featureCollection(outputFeatures), null, 2))
