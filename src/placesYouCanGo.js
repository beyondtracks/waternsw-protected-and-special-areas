#!/usr/bin/env node

const fs = require('fs')
const path = require('path')
const buffer = require('@turf/buffer').default
const combine = require('@turf/combine').default
const union = require('@turf/union').default
const dissolve = require('turf').dissolve
const featureCollection = require('@turf/helpers').featureCollection

const bufferRadius = 50 // in meters

// route GeoJSON features
const routeFeatureFiles = fs.readdirSync(path.join('build', 'route-features'))
const routeFeatures = routeFeatureFiles.map((routeFeatureFile) => {
    console.log(`Read build/route-features/${routeFeatureFile}`)
    return JSON.parse(fs.readFileSync(path.join('build', 'route-features', routeFeatureFile)))
})
console.log(`Read ${routeFeatures.length} routes`)

const sourceGeoJSON = JSON.parse(fs.readFileSync(path.join('data', 'places-you-can-go.geojson')))

// linear GeoJSON data
const linearGeoJSONFeatures = sourceGeoJSON.features.filter((feature) => {
    return feature.geometry.type === 'LineString'
})
console.log(`Read ${linearGeoJSONFeatures.length} line features from GeoJSON`)

// polygon GeoJSON data
const polygonGeoJSONFeatures = sourceGeoJSON.features.filter((feature) => {
    return feature.geometry.type === 'Polygon'
})
console.log(`Read ${polygonGeoJSONFeatures.length} polygon features from GeoJSON`)

// buffer line features
console.log('buffer routes and lines')
const bufferedLines = [...routeFeatures, ...linearGeoJSONFeatures].map((feature) => {
    return buffer(feature, bufferRadius, { units: 'meters' })
})
console.log(`Created ${bufferedLines.length} buffered features`)

console.log('Creating build/places-you-can-go.geojson')
// const output = union(...bufferedLines, ...polygonGeoJSONFeatures)
// const output = featureCollection([union(...bufferedLines, ...polygonGeoJSONFeatures)])
// const output = featureCollection([...bufferedLines, ...polygonGeoJSONFeatures])
const output = dissolve(featureCollection([...bufferedLines, ...polygonGeoJSONFeatures]))

fs.writeFileSync(path.join('build', 'places-you-can-go.geojson'), JSON.stringify(output, null, 2))
