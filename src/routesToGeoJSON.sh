#!/bin/sh

input=$1
output=$2
mkdir -p $output

i=0
for f in $input/*; do
    i=$(($i + 1))
    cat "$f" | jq '{ type: "Feature", properties: {}, geometry: .routes[0].geometry }' > $output/$i.geojson
done
