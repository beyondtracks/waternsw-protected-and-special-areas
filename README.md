# WaterNSW Protected and Special Areas Data

## Background
In NSW exist areas known as [Protected and Special Areas](https://www.waternsw.com.au/water-quality/catchment/manage/special-areas) which you are legally restricted from acessing for certain activities.

According to WaterNSW,

> The majority of the Special Areas are off-limits or restricted to the general public. They are divided into three protection zones with different restrictions to what you can and can't do:
> - Special Areas - no entry
> - Controlled Areas - no entry    
> - Special Areas - restricted entry    
>
> Restrictions do not apply to privately owned land and public roads within the Special Areas.

WaterNSW publish an [online map](https://www.waternsw.com.au/water-quality/catchment/manage/special-areas) and a [PDF map](https://www.waternsw.com.au/__data/assets/pdf_file/0008/121301/Map1-FINAL-what-you-can-and-cant-do-in-special-areas-2016-2.pdf).

WaterNSW have through their customer service chanel provided a GIS shapefile of the boundaries.

## This Repository
This repository aims to process the provided data from WaterNSW into a simple GeoJSON file incorporating permitted areas within the protected and special areas:

- "where you can go" documented at https://www.waternsw.com.au/water-quality/catchment/manage/special-areas/where-you-can-go
  - places-you-can-go-routes.txt contains start/end coordinates allowed of routes
  - places-you-can-go.geojson contains line features to be buffered and polygon features
- privately owned land
- public roads

## Directories

- `data` source
- `build` intermediate build outputs
- `dist` final output

## Running

    make build/routes build/route-features build/places-you-can-go.geojson build/Special_and_Controlled_Areas.geojson dist/Special_and_Controlled_Areas.geojson

Or download the latest [Special_and_Controlled_Areas.geojson](https://gitlab.com/beyondtracks/waternsw-protected-and-special-areas/-/jobs/artifacts/master/raw/dist/Special_and_Controlled_Areas.geojson?job=build).

## License

Licensed BSD-2-Clause, with the exception of data/Special_and_controlled_Areas.zip which is © State of New South Wales through WaterNSW.

